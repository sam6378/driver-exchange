export interface User {
  id: number,
  type: string,
  fullname: string,
  foodCategories?: string[]
}
