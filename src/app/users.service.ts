import { Injectable } from '@angular/core';
import * as Rx from 'rxjs';

import { DataConnectorInterface } from './data-connector-interface';
import { AbstractDataConnector } from './abstract-data-connector';
import { AuthService } from './auth.service';

import { User } from './user'

@Injectable()
export class UsersService extends AbstractDataConnector {

  constructor(
    private authService: AuthService
  ) {
    super('users');
  }

  addFoodCategory(foodCategory: string): Promise<any> {
    return super.get(this.authService.loggedInUserId).first().toPromise()
      .then((user: User) => {
        if(user.foodCategories && user.foodCategories.indexOf(foodCategory) > -1) { return Promise.reject('Food category already set') }
        if(!user.foodCategories) {
          user.foodCategories = [foodCategory];
        } else {
          user.foodCategories.push(foodCategory);
        }
        return super.set(user, this.authService.loggedInUserId);
      });
  }

  removeFoodCategory(foodCategory: string): Promise<any> {
    return super.get(this.authService.loggedInUserId).first().toPromise()
      .then((user: User) => {
        if(!user.foodCategories || (user.foodCategories
          && user.foodCategories.indexOf(foodCategory) === -1)) {
          return Promise.reject('Food category not found');
        }
        user.foodCategories.splice(user.foodCategories.indexOf(foodCategory), 1);
        return super.set(user, this.authService.loggedInUserId);
      })
  }

  getAllFoodCategories(): Rx.Observable<Set<string>> {
    return super.all()
      .map(result => {
        let foodCategories = new Set();
        result.forEach((user: User) => {
          if(user.foodCategories){
            user.foodCategories.forEach((foodCategory: string) => {
              foodCategories.add(foodCategory);
            });
          }
        })
        return foodCategories;
      });
  }
}
