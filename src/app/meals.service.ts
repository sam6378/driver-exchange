import { Injectable } from '@angular/core';
import * as Rx from 'rxjs';

import { AbstractDataConnector } from './abstract-data-connector';

import { Meal } from './meal';

import { UsersService } from './users.service';

@Injectable()
export class MealsService extends AbstractDataConnector {

  constructor(
    private usersService: UsersService
  ) {
    super('meals');
  }

  getFoodCategories(): Rx.Observable<any> {

    return super.all()
      .map(result => {
        let foodCategories = new Set();
        result.forEach((meal: Meal) => {
          meal.foodCategory.forEach((foodCategory: string) => {
            foodCategories.add(foodCategory);
          })
        })
        return foodCategories;
      })
  }

  getRecommendedMeals(): Rx.Observable<Meal[]> {

    return Rx.Observable.combineLatest([
        this.usersService.getAllFoodCategories(),
        super.all()
      ])
      .map(result => this.processMealRecomendations(result[0], JSON.parse(JSON.stringify(result[1]))))
  }

  processMealRecomendations(preferences: Set<string>, meals: Meal[]) {

    let foodCategoryPriorities = ['Nut Free', 'Vegan', 'Diary Free', 'Vegetarian', 'Eggs Free'];
    let foodRecomendations = [];

    foodCategoryPriorities.forEach((foodCategory: string) => {
      if(preferences.has(foodCategory) && foodRecomendations.length < 3) {
        let mealToPush = this.getRandomFoodByCategory(meals, foodCategory);
        meals.splice(meals.findIndex((meal: Meal) => meal.id === mealToPush.id), 1);
        foodRecomendations.push(mealToPush);
      }
    });

    while(foodRecomendations.length < 3) {
      let mealToPush = this.getRandomFoodByCategory(meals);
      meals.splice(meals.findIndex((meal: Meal) => meal.id === mealToPush.id), 1);
      foodRecomendations.push(mealToPush);
    }

    return foodRecomendations;
  }

  getRandomFoodByCategory(meals: Meal[], category: string = undefined) {

    let mealsFiltered = category ? meals.filter((meal: Meal) => meal.foodCategory.find((foodCategory: string) => foodCategory === category) !== undefined) : meals;
    return mealsFiltered[Math.floor(Math.random() * mealsFiltered.length)];
  }
}
