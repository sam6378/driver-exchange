import { TestBed, inject } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';

import { AuthGuard } from './auth-guard.service';

import { AuthService } from './auth.service';

describe('AuthGuardService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [ RouterTestingModule ],
      providers: [ AuthGuard, AuthService ]
    });
  });

  // it('should ...', inject([AuthGuard], (service: AuthGuard) => {
  //   expect(service).toBeTruthy();
  // }));
});
