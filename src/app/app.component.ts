import { Component } from '@angular/core';
import { Router } from '@angular/router';

declare var window;

import { data } from '../assets/data';

import { UsersService } from './users.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  constructor(
    private router: Router,
    private usersService: UsersService
  ) {
    if(!window.localStorage.getItem('users')){
      window.localStorage.setItem('users', JSON.stringify(data.users));
    }
    if(!window.localStorage.getItem('meals')){
      window.localStorage.setItem('meals', JSON.stringify(data.meals));
    }
  }
}
