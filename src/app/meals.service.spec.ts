import { TestBed, inject } from '@angular/core/testing';

import { AuthService } from './auth.service';
import { MealsService } from './meals.service';
import { UsersService } from './users.service';

let dummyMeals = [
  {
    "id": 1,
    "name": "Sweetcorn Chilli and Chowder, Homous, Feta and Roast Vegetable Wrap",
    "foodCategory": ["Vegetarian", "Diary Free"]
  },
  {
    "id": 2,
    "name": "Tandoori Salmon with Bharji Crust",
    "foodCategory": ["Diary Free", "Eggs Free"]
  },
  {
    "id": 3,
    "name": "Chickpea Pancake with Cream Cheese Roast Tomatoes, and Asparagus",
    "foodCategory": ["Vegetarian", "Vegan", "Nut Free"]
  },
  {
    "id": 4,
    "name": "Lamb Kofta with Garlic Yogurt",
    "foodCategory": ["Nut Free"]
  },
  {
    "id": 5,
    "name": "Roast Aubergines with Harissa Spiced Chipeas",
    "foodCategory": ["Vegetarian", "Diary Free"]
  },
  {
    "id": 6,
    "name": "Orange and Hazel Glazed Nut Cornfed Chicken Supreme",
    "foodCategory": ["Diary Free"]
  },
  {
    "id": 7,
    "name": "Roast Peppers with Grilled Halloumi, Olives and Capers",
    "foodCategory": ["Vegetarian"]
  },
  {
    "id": 8,
    "name": "Blue Cheese Beef Burger	Blackbean",
    "foodCategory": ["Nut Free", "Eggs Free"]
  },
  {
    "id": 9,
    "name": "Sweet Potato Burger with Blue Cheese",
    "foodCategory": ["Vegetarian", "Nut Free", "Eggs Free"]
  }
];

describe('MealsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ AuthService, MealsService, UsersService ]
    });
  });

  it('should given a food preferences return a meal recommendation', inject([MealsService], (service: MealsService) => {
    let result = service.getRandomFoodByCategory(dummyMeals, 'Nut Free');
    expect(result.foodCategory).toContain('Nut Free');
  }));
});
