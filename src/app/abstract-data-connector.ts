import * as Rx from 'rxjs';

export abstract class AbstractDataConnector {

  subject: Rx.BehaviorSubject<any[]>;

  constructor(
    private entity: string
  ) {
    this.subject = new Rx.BehaviorSubject(JSON.parse(window.localStorage.getItem(this.entity)));
  }

  get(id: number): Rx.Observable<any> {
    return this.subject.asObservable()
      .flatMap(data => data)
      .find(data => data.id === id);
  }

  set(data: any, id?: number): Promise<any> {
    let dataFromStorage = JSON.parse(window.localStorage.getItem(this.entity));

    if(id){
      let index = dataFromStorage.findIndex(data => data.id === id);
      if(index === -1) { Promise.reject('Not Found') };
      dataFromStorage[index] = data;
    } else {
      let existingIds = dataFromStorage.map(data => data.id);
      data.id = existingIds.length ? Math.max.apply(null, existingIds) + 1 : 1;
      dataFromStorage.push(data);
    }

    window.localStorage.setItem(this.entity, JSON.stringify(dataFromStorage));
    this.subject.next(dataFromStorage);
    return Promise.resolve(data);
  }

  all(query?: any): Rx.Observable<any[]> {
    return this.subject.asObservable()
      .map(data => query ? this.query(data, query) : data);
  }

  private query(data, query){
    Object.keys(query).forEach(key => {
      data = data.filter(row => row[key] === query[key]);
    });
    return data;
  }
}
