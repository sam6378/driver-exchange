import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import * as Rx from 'rxjs';

import { AuthService } from '../auth.service';
import { UsersService } from '../users.service';

import { User } from '../user';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  users: Rx.Observable<User[]>;
  chefs: Rx.Observable<User[]>;
  loginsubscription: Rx.Subscription;

  constructor(
    private authService: AuthService,
    private router: Router,
    private usersService: UsersService
  ) { }

  ngOnInit() {
    this.users = this.usersService.all({type: 'USER'});
    this.chefs = this.usersService.all({type: 'CHEF'});
  }

  ngOnDestroy() {
    this.loginsubscription.unsubscribe();
  }

  login(user: User) {
    this.loginsubscription = this.authService.login(user).subscribe(() => {
      if (this.authService.loggedInUserId) {
        this.router.navigate([(user.type === 'CHEF' ? 'chef' : 'user') + '/' + user.id]);
      }
    });
  }
}
