import { TestBed, inject } from '@angular/core/testing';

import { AuthService } from './auth.service';
import { UsersService } from './users.service';

describe('UsersService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [UsersService, AuthService]
    });
  });

  // it('should ...', inject([UsersService], (service: UsersService) => {
  //   expect(service).toBeTruthy();
  // }));
});
