export interface Meal {
  id: number,
  name: string,
  foodCategory: string[];
}
