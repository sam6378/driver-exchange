import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import * as Rx from 'rxjs';

import { MealsService } from '../../meals.service';
import { UsersService } from '../../users.service';

import { User } from '../../user';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {

  userFoodCategories: Rx.Observable<string[]>;
  foodCategories: Rx.Observable<string[]>;

  constructor(
    private activatedRoute: ActivatedRoute,
    private mealsService: MealsService,
    private usersService: UsersService
  ) { }

  ngOnInit() {
    this.foodCategories = this.mealsService.getFoodCategories();
    this.activatedRoute.params.subscribe(params => {
      this.userFoodCategories = this.usersService.get(parseInt(params.id))
        .map(data => {
          if(!data.foodCategories) {data.foodCategories = []}
          return data.foodCategories;
        });
    });
  }

  addFoodCategory(foodCategory: string): Promise<User> {
    return this.usersService.addFoodCategory(foodCategory)
      .catch(console.error);
  }

  removeFoodCategory(foodCategory: string): Promise<User> {
    return this.usersService.removeFoodCategory(foodCategory)
      .catch(console.error);
  }
}
