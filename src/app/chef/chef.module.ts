import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ChefComponent } from './chef/chef.component';

import { ChefRoutingModule } from './chef-routing.module';

@NgModule({
  imports: [
    CommonModule,
    ChefRoutingModule
  ],
  declarations: [ChefComponent]
})
export class ChefModule { }
