import { Component, OnInit } from '@angular/core';
import * as Rx from 'rxjs';

import { MealsService } from '../../meals.service';
import { UsersService } from '../../users.service';

import { Meal } from '../../meal';

@Component({
  selector: 'app-chef',
  templateUrl: './chef.component.html',
  styleUrls: ['./chef.component.css']
})
export class ChefComponent implements OnInit {

  userFoodCategories: Rx.Observable<Set<string>>;
  recommendedMeals: Rx.Observable<Meal[]>;

  constructor(
    private usersService: UsersService,
    private mealsService: MealsService
  ) { }

  ngOnInit() {
    this.userFoodCategories = this.usersService.getAllFoodCategories();
    this.recommendedMeals = this.mealsService.getRecommendedMeals();
  }
}
