import { Injectable } from '@angular/core';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/delay';

@Injectable()
export class AuthService {
  loggedInUserId: number = 0;

  login(user): Observable<boolean> {
    return Observable.of(true).do(val => this.loggedInUserId = user.id);
  }

  logout(): void {
    this.loggedInUserId = 0;
  }
}
