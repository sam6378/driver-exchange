export interface DataConnectorInterface {
  get(id: number): Promise<any>;
  set(id: number, data: any): Promise<any>;
  all(): Promise<any[]>;
}
