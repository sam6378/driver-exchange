import { DriverExchangeCodeChallengePage } from './app.po';

describe('driver-exchange-code-challenge App', () => {
  let page: DriverExchangeCodeChallengePage;

  beforeEach(() => {
    page = new DriverExchangeCodeChallengePage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
